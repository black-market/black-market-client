#!/usr/bin/python
import sys
import random
import time
import json
import base64
import hashlib
import pycurl
import os.path
from M2Crypto.util import passphrase_callback as prompt_password

import argspar
import crypto

keypair_filename = '.blackmarket3.rsa'

class ServerError(Exception):
    def __init__(self, ret):
        self.ret = ret

    def __str__(self):
        return 'Server error: %s'%self.ret

class Problem(Exception):
    def __init__(self, errcode, errmsg, params):
        self.errmsg = errmsg

    def __str__(self):
        return self.errmsg

class UserError(Exception):
    def __init__(self, errmsg):
        self.errmsg = errmsg

    def __str__(self):
        return self.errmsg

class Requester:
    def __init__(self, access):
        self.access = access
#        self.base_url = 'http://dev.glbse.com:9999'
        self.base_url = 'https://glbse.com/GLBSE'
        #self.base_url = 'http://195.200.253.242:4567'
        #self.base_url = 'http://localhost:9393'
        self.curl = pycurl.Curl()
        self.curl.setopt(pycurl.POST, 1)
        self.curl.setopt(pycurl.FOLLOWLOCATION, 1)
        self.curl.setopt(pycurl.VERBOSE, 0)
        self.curl.setopt(pycurl.SSL_VERIFYHOST, 0)
        #self.curl.setopt(pycurl.SSL_VERIFYHOST, 0)
        self.curl.setopt(pycurl.SSL_VERIFYPEER, 0)
        #self.curl.setopt(pycurl.CAINFO, "server.pem")
        #self.curl.setopt(pycurl.CRINFO, "server.crt")#CRL
        #self.curl.setopt(curl.CAINFO, "/path/to/CA.crt")
        #self.curl.setopt(pycurl.SSLCERT, "server.pem")
        self.curl.setopt(pycurl.TIMEOUT, 500)
        self.curl.setopt(pycurl.CONNECTTIMEOUT, 300)
        self.buff = ""
        self.curl.setopt(pycurl.WRITEFUNCTION, self.write_buffer)

    def perform(self, method, data):
        self.curl.setopt(pycurl.HTTPPOST, self.prepare(data))
        url = '%s/%s'%(self.base_url, method)
        self.curl.setopt(pycurl.URL, url)

        self.curl.perform()
        #response = self.curl.getinfo(pycurl.RESPONSE_CODE)
        status = self.curl.getinfo(pycurl.HTTP_CODE)
        if status == "OK" or status == 200:
            #print(self.buff)
            ret = base64.b64decode(self.buff)
        else:
            ret = self.buff

            #write output to a file
            text_file = open("output.debug.html", "w")
            text_file.write(ret)
            text_file.close()

            raise ServerError(ret)
        self.curl.close()
        return ret

    def prepare(self, data):
        userid = self.access.user_id()
        nonce = random.randint(0, sys.maxint)
        data['nonce'] = nonce
        data['userid'] = userid
        data = json.dumps(data)
        data = base64.b64encode(data)
        signature = self.access.sign(data)
        signature = base64.b64encode(signature)
        return [('data', data), ('signature', signature)]

    def write_buffer(self, ret):
        self.buff += ret

def load(filename):
    if filename == 'STDIN':
        return sys.stdin.read()
    return open(filename).read()

def save(filename, stream):
    open(filename, 'w').write(stream)

def nl():
    print '*******************************************************************************************'

class Branches(object):


    def register(self, args):
        # create new keypair
        access = crypto.AccessCard()
        access.generate()
        # start server request sequence
        req = Requester(access)
        data = {'public_key': access.public_key()}
        ret = req.perform('register', data)
        # sanity check.
        assert(json.loads(ret)['public_id'] == data['userid'])
        # prompt for password
        passw = prompt_password(True)
        priv_key = access.private_key(passw)
        # store the keypair locally
        save(self.keypair_filename, priv_key)
        print "Registered. Keep the file named %s safe."%self.keypair_filename

    def puts(self,struc):
        #print '*******************************************************************************************'

        print " "
        for key in struc.iterkeys():
            print key,"--:--", struc[key]

    def user(self, args):
        res=self.access.user_id()
        self.puts({'user':res})

#    def newkeys(self,args):
#        ret = self.request('generate_keypairs',{})
#        self.puts(ret)

    def list_assets(self,args):
        ret = self.request('list_all_assets',{})
        self.puts(ret)

    def deposit(self, args):
        ret = self.request('account/deposit', {})
        addy = ret['address']
        print 'Deposit funds to', addy
		#done
    def withdraw(self, args):
        data = {'amount': args.amount,'address': args.destaddy}
        ret = self.request('account/withdraw', data)
        self.puts(ret)

#balance --:-- 3.0
#result --:-- True


    def balance(self, args):
        ret = self.request('account/balance', {})
        self.puts(ret)

		#done
    def new_asset(self, args):
        cont = load(args.contract_filename)
        # sign the contract + encode
        cont_sig = self.access.sign(cont)
        cont_sig = base64.b64encode(cont_sig)
        # build the data required for the request
        data = {'contract': cont, 'contract_signature': cont_sig}
        ret = self.request('account/asset/new', data)
        nl()
        print 'Created new asset:', ret['asset_id']
        print "You will now need to issue contracts using the 'issue' command."
		#done
    def issue(self, args):
        if args.quantity <= 0:
            raise UserError('Invalid quantity to issue.')
        data = {'quantity': args.quantity, 'asset_id': args.asset_id}
        ret = self.request('account/asset/issue', data)
        nl()
        print 'Issued %d contracts. %d in total.'%(args.quantity, ret['balance'])

    def asset_transfer(self, args):
        data = {
            'quantity': args.quantity,
            'asset': args.asset_id,
            'to_account': args.destid
        }
        ret = self.request('account/asset/transfer', data)

        #need to test output
        self.puts(ret)



    def bitcoin_transfer(self, args):
        data = {
            'amount': args.amount,
            'to_account': args.destid
        }
        ret = self.request('account/bitcoin/transfer', data)

        #need to test output
        self.puts(ret)


    def folio(self, args):
        ret = self.request('account/asset/list', {})
        #self.puts(ret['assets'])
        first_loop = True
        ass_len = 0
        for assid, data in ret['assets'].iteritems():
            if first_loop:
                ass_len = len(assid)
                print '', '-'*(3 + ass_len + 10)
                print '| Asset ID', ' '*(len(assid) - 9), '| Issues   |'
                print '|%s|'%('-'*(3 + len(assid) + 10))
                first_loop = False
            balance = data['balance']
           # number of digits or length of string repr
            balslen = len(str(balance))
            print '|', assid, '|', ' '*(7 - balslen), balance, '|'
        if not first_loop:
            print '', '-'*(3 + ass_len + 10)

    def asset(self, args):
        ret = self.request('asset',{'asset_id':args.asset_id})
        self.puts(ret)


    def ticker(self,args):
        data={
        'asset_id':args.asset_id,
        'ticker':args.ticker
        }
        ret = self.request('account/asset/register',data)
        self.puts(ret)

		#needs pretty print
    def buy(self, args):
        data = {
            'quantity': args.quantity,
            'asset': args.asset_id,
            'price': args.price
        }

        ret = self.request('market/buy', data)
        self.puts(ret)
		#needs pretty print
    def sell(self, args):
        data = {
            'quantity': args.quantity,
            'asset': args.asset_id,
            'price': args.price
        }

        ret = self.request('market/sell', data)
        self.puts(ret)

    def cancel(self, args):
        ret = self.request('market/order/cancel', {'order_id':args.order_id})
        self.puts(ret)

    def depth(self, args):
        ret = self.request('asset/depth',{'asset_id':args.asset_id})
        self.puts(ret)

    def orders(self, args):
        ret = self.request('market/orders', {})
        res=ret['orders']
        for k in res:
            tmp=res[k]
            tmp['order_number']=k
            self.puts(tmp)#print k

    def bitcoin_history(self,args):
        ret = self.request('history/bitcoin',{'entries':args.entries})

        res=ret['transactions']
        for k in res:
            tmp=res[k]
            tmp['transaction']=k
            self.puts(tmp)

    def asset_history(self,args):
        ret = self.request('history/assets',{'entries':args.entries})
        res=ret['transactions']
        for k in res:
            tmp=res[k]
            tmp['transaction']=k
            self.puts(tmp)

    def market_history(self,args):
        ret = self.request('history/market',{'entries':args.entries})
        res=ret['transactions']
        for k in res:
            tmp=res[k]
            tmp['transaction']=k
            self.puts(tmp)

    def records(self,args):
        ret=self.request('account/records',{'entries':args.entries})
        res=ret['records']
        for k in res:
            print k

    def new_vote(self,args):
        mot = load(args.vote_motion_filename)
        data={
            'motion':mot,
            'asset_id':args.asset_id,
            'date':args.to_date
        }
        ret = self.request('vote/new',data)
        print ret

    def vote(self,args):
        data={
            'vote':args.ballot,
            'vote_id':args.vote_id
            }
        ret=self.request('vote/register',data)
        print ret

    def tally(self,args):
        ret=self.request('vote/tally',{'motion_id':args.motion_id})
        self.puts(ret)

    def motion(self,args):
        ret=self.request('vote/asset/motion',{'motion_id':args.motion_id})
        self.puts(ret)

    def motions(self,args):
        ret=self.request('vote/motions',{})
        print ret


    def pay(self,args):
        data={
            'amount':args.amount,
            'asset_id':args.asset_id
        }
        ret=self.request('account/asset/pay',data)
        print ret


    def recall(self,args):
        data={
            'amount':args.amount,
            'asset_id':args.asset_id
        }
        ret=self.request('account/asset/recall',data)
        print ret

class Plexer(Branches):
    def request(self, path, data):
        req = Requester(self.access)
        ret = req.perform(path, data)

        ret = json.loads(ret)
        if 'errcode' in ret.keys():
            raise Problem(ret['errcode'], ret['errmsg'], ret['params'])
        else:
            return ret

    def load_access(self):
        pem = load(self.keypair_filename)
        # prompt for password
        passw = prompt_password(False)
        # load old keypair
        access = crypto.AccessCard()
        access.load(pem, passw)
        return access

    def branch(self, args):
        if args.command != "register":
            self.access = self.load_access()
        else:
            self.access = None
        return getattr(self, args.command)(args)

    @property
    def keypair_filename(self):
        return os.path.join(os.path.expanduser("~"), keypair_filename)

    def run(self):
        args = argspar.parse()
        try:
            self.branch(args)
        except crypto.RSA.RSAError:
            sys.stderr.write('Bad password for keypair.\n')
        except ServerError as se:
            try:
                ret = json.loads(se.ret)['errmsg']
            except ValueError:
                ret = se.ret
            sys.stderr.write('Server error: %s.'%ret)
        except Problem as prob:
            sys.stderr.write('Problem: %s\n'%prob.errmsg)
        except UserError as ue:
            sys.stderr.write('User error: %s\n'%ue.errmsg)
        except pycurl.error as pyerr:
            sys.stderr.write('Server error: %s.\n'%pyerr[1])

if __name__ == '__main__':
    random.seed(time.time())
    #sys.argv.append('balance')
    app = Plexer()
    app.run()

