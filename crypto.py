#!/usr/bin/python                 
from M2Crypto import RSA, BIO
import hashlib

class NoRSALoaded(Exception):
    def __str__(self):
        return 'No RSA was loaded.'

def passcb_factory(passphrase):
    cipher = 'aes_256_cbc'
    if not passphrase:
        cipher = None
    return cipher, lambda *args: passphrase

class AccessCard:
    def __init__(self):
        self.keypair = None

    def generate(self):
        self.keypair = RSA.gen_key(2048, 65537)

    def load(self, pemstr, passphrase):
        passcb = passcb_factory(passphrase)[1]
        self.keypair = RSA.load_key_string(pemstr, passcb)
        if self.keypair is None:
            raise NoRSALoaded()

    def public_key(self):
        if self.keypair is None:
            raise NoRSALoaded()
        bio = BIO.MemoryBuffer()
        self.keypair.save_pub_key_bio(bio)
        return bio.read_all()

    def private_key(self, passphrase):
        if self.keypair is None:
            raise NoRSALoaded()
        bio = BIO.MemoryBuffer()
        cipher, passcb = passcb_factory(passphrase)
        self.keypair.save_key_bio(bio, cipher=cipher, callback=passcb)
        return bio.read_all()

    def sign(self, data):
        if self.keypair is None:
            raise NoRSALoaded()
        digest = hashlib.sha512(data).digest()
        return self.keypair.sign(digest, 'sha512')

    def user_id(self):
        pk = self.public_key()
        return hashlib.sha256(pk).hexdigest()

if __name__ == '__main__':
    rsa = AccessCard()
    rsa.generate('hello')
    print rsa.public_key()
    privk = rsa.private_key('hello')
    print rsa.sign('you are sucker')
    print privk
    rsa2 = AccessCard()
    rsa2.load(privk, 'hello')

