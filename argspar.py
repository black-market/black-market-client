import sys
import argparse
import decimal

############
# commands #
############
#register
#user

#balance
#deposit
#withdraw

#new-asset
#issue
#transfer
#folio (as in, my portfolio)
#asset (gets last trade price, highest bid, lowest ask,issues
#ticker register a ticker symbol for an asset

#buy
#sell
#cancel
#depth
#orders

#bitcoin-history
#asset-history
#market-history

#vote
#new-vote
#motions
#pay-div pay dividends


def numstr_to_internal(numstr):
    return int(numstr)#decimal.Decimal(numstr) * 10**8)

class Parser:
    def __init__(self):
        self.parser = argparse.ArgumentParser(
                description='Bitcoin stock market client.',
                epilog='See glbse.com for more info.')
        self.parser.add_argument('--version', '-v', action='version',
                version='%(prog)s 2.0')
        self.subparsers = self.parser.add_subparsers(title='actions',
                help='Use <action> -h for more info.')

        register = self.subparsers.add_parser('register',
                help='Creates a new account for the client on the server.')
        register.set_defaults(command='register')

        user = self.subparsers.add_parser('user',
                help='Display your user-ID and exit.')
        user.set_defaults(command='user')

        balance = self.subparsers.add_parser('balance',
                help='Show bitcoin balance.')
        balance.set_defaults(command='balance')

        deposit = self.subparsers.add_parser('deposit',
                help='Return address to deposit bitcoins.')
        deposit.set_defaults(command='deposit')

        withdraw = self.subparsers.add_parser('withdraw',
                help='Withdraw bitcoins.')
        withdraw.add_argument('amount', metavar='AMOUNT', type=numstr_to_internal,
                help='Amount to withdraw.')
        withdraw.add_argument('destaddy', metavar='DEST',
                help='Address to send bitcoins to.')
        withdraw.set_defaults(command='withdraw')

        new_asset = self.subparsers.add_parser('new-asset',
                help='Creates a new asset.')
        new_asset.add_argument('contract_filename', nargs='?',
                metavar='[FILE]', default='STDIN',
                help='Filename for contract. Reads from STDIN if not specified.')
        new_asset.set_defaults(command='new_asset')

        issue = self.subparsers.add_parser('issue',
                help='Issue units of an asset.')
        issue.add_argument('quantity', metavar='N', type=int,
                help='Quantity of units of the asset to issue.')
        issue.add_argument('asset_id', metavar='ASSET', help='Asset ID.')
        issue.set_defaults(command='issue')

        asset_transfer = self.subparsers.add_parser('asset-transfer',
                help='Transfer a quantity of assets.')
        asset_transfer.add_argument('quantity', metavar='N', type=int,
                help='Quantity of the asset.')
        asset_transfer.add_argument('asset_id', metavar='ASSET', help='Asset ID.')
        asset_transfer.add_argument('destid', metavar='DESTID',
                help='User ID to transfer to.')
        asset_transfer.set_defaults(command='asset_transfer')



        bitcoin_transfer = self.subparsers.add_parser('bitcoin-transfer',
                help='Instantly transfer bitcoin to another account.')
        bitcoin_transfer.add_argument('amount', metavar='N', type=int,
                help='Amount of bitcoin to send.')
        bitcoin_transfer.add_argument('destid', metavar='DESTID',
                help='User ID to transfer to.')
        bitcoin_transfer.set_defaults(command='bitcoin_transfer')


        folio = self.subparsers.add_parser('folio',
								help='Your asset portfolio, lists all your assets and their quantities.')
        folio.set_defaults(command='folio')


        asset = self.subparsers.add_parser('asset',
			help='Show the last trade,highest bid, lowest ask, and total number of issues for an asset.')
        asset.add_argument('asset_id', metavar='ASSET', help='Asset ID.')
        asset.set_defaults(command='asset')

        ticker = self.subparsers.add_parser('ticker',
            help='Register a ticker symbol for an asset')
        ticker.add_argument('asset_id', metavar='ASSET', help='Asset ID.')
        ticker.add_argument('ticker', metavar='TICKER', help='Ticker Symbol.')
        ticker.set_defaults(command='ticker')


        buy = self.subparsers.add_parser('buy', help='Attempt to buy an asset.')
        buy.add_argument('quantity', metavar='N', type=int,
                help='Quantity of the asset.')
        buy.add_argument('asset_id', metavar='ASSET', help='Asset ID.')
        buy.add_argument('price', metavar='PRICE', type=numstr_to_internal,
                help='Price per issue.')
        buy.set_defaults(command='buy')

        sell = self.subparsers.add_parser('sell', help='Sell asset.')
        sell.add_argument('quantity', metavar='N', type=int,
                help='Quantity of the asset.')
        sell.add_argument('asset_id', metavar='ASSET', help='Asset ID.')
        sell.add_argument('price', metavar='PRICE', type=numstr_to_internal,
                help='Price per issue.')
        sell.set_defaults(command='sell')

        cancel = self.subparsers.add_parser('cancel',
                help='Cancel an in-progress order.')
        cancel.add_argument('order_id', metavar='ORDER', help='Order ID.')
        cancel.set_defaults(command='cancel')

        depth = self.subparsers.add_parser('depth',
                help='Get the bids and asks for an asset.')
        depth.add_argument('asset_id', metavar='ASSET', help='Asset ID.')
        depth.set_defaults(command='depth')

        orders = self.subparsers.add_parser('orders',
                help='List all orders not fulfilled.')
        orders.set_defaults(command='orders')

        bitcoin_history = self.subparsers.add_parser('bitcoin-history',
                help='Show all bitcoin transactions.')
        bitcoin_history.add_argument('entries', metavar='ENTRIES', help='Number of entries to show in history')

        bitcoin_history.set_defaults(command='bitcoin_history')


        asset_history = self.subparsers.add_parser('asset-history',
                help='Show all asset transactions.')
        asset_history.add_argument('entries', metavar='ENTRIES', help='Number of entries to show in history')
        asset_history.set_defaults(command='asset_history')

        market_history = self.subparsers.add_parser('market-history',
                help='Show all your trades.')
        market_history.add_argument('entries', metavar='ENTRIES', help='Number of entries to show in history')

        market_history.set_defaults(command='market_history')


        records = self.subparsers.add_parser('records',
                help='Show all bitcoin, market, and asset transactions (deposits/withdrawls/trades/transfers/fees).')
        records.add_argument('entries', metavar='ENTRIES', help='Number of entries to show in history')

        records.set_defaults(command='records')


        new_vote = self.subparsers.add_parser('new-vote',
                help='Create a new motion to be voted on by asset holders.')
        new_vote.add_argument('asset_id', metavar='ASSET', help='Asset ID.')
        new_vote.add_argument('vote_motion_filename', nargs='?',
                metavar='[FILE]', default='STDIN',
                help='Filename for vote motion. Reads from STDIN if not specified.')
        new_vote.add_argument('to_date', metavar='DATE', help='The date voting is closed. dd-mm-yy')
        new_vote.set_defaults(command='new_vote')

        vote = self.subparsers.add_parser('vote',
                help='Vote on asset motions.')
        vote.add_argument('vote_id', metavar='VOTEID', help='Motion ID.')
        vote.add_argument('ballot', metavar='BALLOT', help='You\'re vote, yes|no.')
        vote.set_defaults(command='vote')

        motion = self.subparsers.add_parser('motion',
            help='Shows motion for asset.')
        motion.add_argument('motion_id', metavar='MOTIONID', help='Motion ID.')
        motion.set_defaults(command='motion')

        motions = self.subparsers.add_parser('motions',
            help='Lists motions for held assets.')
        motions.set_defaults(command='motions')

        tally = self.subparsers.add_parser('tally',
            help='Show the voting results for motion.')
        tally.add_argument('motion_id',metavar='MOTIONID', help='Motion ID.')
        tally.set_defaults(command='tally')

        pay = self.subparsers.add_parser('pay',
            help='Make payments to asset holders.')
        pay.add_argument('asset_id', metavar='ASSET', help='Asset ID.')
        pay.add_argument('amount', metavar='AMOUNT',
            help='The amount to be paid, will be split among all asset holders.')
        pay.set_defaults(command='pay')


        recall = self.subparsers.add_parser('recall',
            help='Recalls and removes assets from the issuers account.')
        recall.add_argument('asset_id', metavar='ASSET', help='Asset ID.')
        recall.add_argument('amount', metavar='N',
            help='Quantity of units to recal.')
        recall.set_defaults(command='recall')

#        keys = self.subparsers.add_parser('newkeys',
#                                          help='Have server generate and return to you a RSA keypair.')
#        keys.set_defaults(command='newkeys')

        list_assets = self.subparsers.add_parser('list-assets',
                                                 help='Lists all assets available on the market')
        list_assets.set_defaults(command='list_assets')





        help = self.subparsers.add_parser('help',
                help='Show this help text or get help for a command.')
        help.add_argument('action', metavar='[ACTION]', nargs='?',
                help='Optional command.')
        help.set_defaults(command='help')

    def run(self):
        args = self.parser.parse_args()
        if args.command == 'help':
            if args.action is not None:
                self.parser.parse_args([args.action, '-h'])
            else:
                self.parser.parse_args(['-h'])
        return args

def parse():
    p = Parser()
    return p.run()

