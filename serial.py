import pickle

class Serialiser:
    def __init__(self, filename):
        self.fname = filename
        try:
            binary = open(filename).read()
            self.dic = pickle.loads(binary)
        except IOError:
            self.dic = {}
        except EOFError:
            self.dic = {}

    def get_keypair(self):
        return self.lookup('keypair')
    def set_keypair(self, val):
        push_back('keypair', val)
    keypair = property(get_keypair, set_keypair)

    def get_aliases(self):
        return self.lookup('aliases')
    def set_aliases(self, val):
        push_back('aliases', val)
    aliases = property(get_aliases, set_aliases)

    def lookup(self, name):
        try:
            return self.dic[name]
        except KeyError:
            return None

    def push_back(self, name, val):
        self.dic[name] = val

    def commit(self):
        stream = pickle.dumps(self.dic)
        open(self.fname, 'w').write(stream)

if __name__ == '__main__':
    s = Serialiser('/tmp/test.ser')
    print s.keypair
    s.keypair = 'hello'
    s.commit()

    z = Serialiser('/tmp/test.ser')
    print z.keypair

